//
//  GameOverScene.swift
//  ZombieConga
//
//  Created by MacStudent on 2019-02-13.
//  Copyright © 2019 Parrot. All rights reserved.
//

import Foundation
import SpriteKit

// The default template for a new scssene
class GameOverScene:SKScene {
    
    var isWin = false
    
    init(size: CGSize, win:Bool) {
        super.init(size: size)
        self.isWin = win
    }
    
    // required nonsense to make this class work
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMove(to view: SKView) {
        
        var background = SKSpriteNode()
        if(isWin == true){
            background = SKSpriteNode(imageNamed: "YouWin")
        }else{
            background = SKSpriteNode(imageNamed: "YouLose")
        }
        
        background.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
        
        addChild(background)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // When person touches screen, send them back to the game
        
        // 1. Initialize the new scene
        let gameScene = GameScene(size:self.size)
        gameScene.scaleMode = self.scaleMode
        
        // 2. Configure the "animation" between screens
        let transitionEffect = SKTransition.flipHorizontal(withDuration: 3)
        
        // 3. Show the scene
        self.view?.presentScene(gameScene, transition: transitionEffect)
        
        
    }
}
