//
//  GameScene.swift
//  ZombieConga
//
//  Created by Parrot on 2019-01-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
   
    
    // MARK: Sprites
    // ----------------------
    let zombie = SKSpriteNode(imageNamed: "zombie1")
    let cat = SKSpriteNode(imageNamed: "cat")
    let enemy = SKSpriteNode(imageNamed:"enemy")
    
    // MARK: Variables dealing with movement
    // ----------------------
    // variables used to make zombie move to mouse
    var xn:CGFloat = 0
    var yn:CGFloat = 0
    
    // speed of zombie
    let ZOMBIE_SPEED:CGFloat = 5
    
    // GAME STATISTICS
    let livesLabel = SKLabelNode(text: "")
    var zombieLives = 3

    let catCollectedLabel = SKLabelNode(text: "")
    var catCollected = 0
    

    // 4:3
    // Screen size (width, height): 2048.0, 1536.0  (16:9 aspect ratio)
    // X-coordinates to be:  (0, 2048)
    // Y-coordinates: (192, 1344)
    // MARK: Playfield boundary variables
    let actualPlayfieldRect:CGRect
    let SHOW_BOUNDARIES = true
    
    override init(size: CGSize) {
        // if this was a 4:3 screen, change it to 4.0/3.0
        let maxAspectRatio:CGFloat = 16.0/9.0
        
        let playfieldHeight = size.width / maxAspectRatio
        let margins = (size.height - playfieldHeight) / 2.0
        
        self.actualPlayfieldRect = CGRect(
            x: 0,
            y: margins,
            width: size.width,
            height: playfieldHeight)

        
        super.init(size:size)
        

    }

    func drawBoundaries() {
        // 1. Create a rectangle
        let rect = SKShapeNode()
        let path = CGMutablePath()
        path.addRect(self.actualPlayfieldRect)
        rect.path = path
        
        // 2. Congfigure the rectangle
        rect.strokeColor = SKColor.cyan
        rect.lineWidth = 10.0
        
        // 3. draw the rectangle on the screen
        addChild(rect)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: Initialize the game here
    override func didMove(to view: SKView) {
        print("Screen width/height: \(self.size.width), \(self.size.height)")
        // Set the background color of the app
        self.backgroundColor = SKColor.black;

        
        // DEBUG: Show playfield boundaries
        if (self.SHOW_BOUNDARIES) {
            self.drawBoundaries()
        }
        
        // MARK: Configure & add the sprites to the screen
        
        
        // zombie
        self.zombie.position = CGPoint(x: 1000, y: 800)
        
        // - add a hitbox to the zombie
        self.zombie.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.zombie.frame.width, height:self.zombie.frame.height))
        
        self.zombie.physicsBody?.affectedByGravity = false
        self.zombie.physicsBody?.isDynamic = false
        
        addChild(zombie)

        // cat
        self.cat.position = CGPoint(x: 400, y: 800)
        
        // - add a hitbox to the cat
        self.cat.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.cat.frame.width, height:self.cat.frame.height))
        
        self.cat.physicsBody?.affectedByGravity = false
        self.cat.physicsBody?.isDynamic = false
        
        addChild(cat)

        // enemey
        self.enemy.position = CGPoint(x: self.size.width-300, y: self.size.height/2)
        addChild(enemy)
        
        
        // GAME STATS
        self.livesLabel.text = "LIVES LEFT: \(self.zombieLives)"
        self.livesLabel.fontName = "AvenirNext-Bold"
        self.livesLabel.fontSize = 70
        self.livesLabel.fontColor = UIColor.cyan
        self.livesLabel.position = CGPoint(x: self.actualPlayfieldRect.minX + 300, y: self.actualPlayfieldRect.maxY - 100)
        addChild(livesLabel)
        
        self.catCollectedLabel.text = "CATS COLLECTED: \(self.catCollected)/5"
        self.catCollectedLabel.fontName = "AvenirNext-Bold"
        self.catCollectedLabel.fontSize = 70
        self.catCollectedLabel.fontColor = UIColor.cyan
        self.catCollectedLabel.position = CGPoint(x: self.actualPlayfieldRect.minX + 440, y: self.actualPlayfieldRect.maxY - 200)
        addChild(catCollectedLabel)
        
        
         self.makeGrandmaMove()
    }
    
    func makeGrandmaMove(){
        // Changing the grandma movement pattern to go random
        let x1 = self.size.width / 2 * -1
        let y1 = self.size.height / 2
        let x2 = x1
        let y2 = y1 * -1
        
        let action1 = SKAction.moveBy(x: x1, y: y1, duration: 3)
        let action2 = SKAction.moveBy(x: x2, y: y2, duration: 3)
        let action3 = action2.reversed()
        let action4 = action1.reversed()
        
        let sequence = SKAction.sequence([action1, action2, action3, action4])
        self.enemy.run(SKAction.repeatForever(sequence))
    }
    
    
    func restartGame() {
        self.zombie.position = CGPoint(x: 1000, y: 800)
        self.cat.position = CGPoint(x: 400, y: 800)
        self.enemy.position = CGPoint(x: self.size.width-300, y: self.size.height/2)
        
        // SET POSITION TO ZERO
        self.xn = 0
        self.yn = 0
        
        self.enemy.removeAllActions()
        self.makeGrandmaMove()
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Changing the zombie movement pattern to go towards the mouse
        self.zombie.position.x = self.zombie.position.x + self.xn*ZOMBIE_SPEED
        self.zombie.position.y = self.zombie.position.y + self.yn*ZOMBIE_SPEED
        

        
        // Make zombie stay INSIDE the screen
        // - Do collision detection against the walls
        // -------------------------------------
        // 1. check against left and right sides
        if (self.zombie.position.x <= 0 ||
            self.zombie.position.x >= self.size.width) {
            self.xn = self.xn * -1
        }
        
        // 2. check top and bottom of screen
        if (self.zombie.position.y <= 0 ||
            self.zombie.position.y >= self.size.height) {
            self.yn = self.yn * -1
        }
        
        
        // MARK: Check if zombie touches enemuy
        // -------------------------------------
        if (self.zombie.frame.intersects(self.enemy.frame)) {
            
            // REDUCE LIVES
            if(self.zombieLives == 1){
                
                //   1. Initialize the new scene
                let gameOverScene = GameOverScene(size:self.size, win:false)
                gameOverScene.scaleMode = self.scaleMode
                
                // 2. Configure the "animation" between screens
                let transitionEffect = SKTransition.flipHorizontal(withDuration: 3)
                
                // 3. Show the scene
                self.view?.presentScene(gameOverScene, transition: transitionEffect)
                
                
            }else{
                self.zombieLives -= 1
                self.livesLabel.text = "LIVES LEFT: \(self.zombieLives)"
            }
            
            // RESTART THE GAME
            // MOVING ALL THE SPRITES BACK TO ORIGINAL POSITION
            self.restartGame()
            
        }
        
        
        // MARK: Check if zombie touches cat
        // -------------------------------------
        if (self.zombie.frame.intersects(self.cat.frame)) {
            print("COLLISION!")
        
            
            if(catCollected == 4){
                //   1. Initialize the new scene
                let gameOverScene = GameOverScene(size:self.size, win:true)
                gameOverScene.scaleMode = self.scaleMode
                
                // 2. Configure the "animation" between screens
                let transitionEffect = SKTransition.flipHorizontal(withDuration: 3)
                
                // 3. Show the scene
                self.view?.presentScene(gameOverScene, transition: transitionEffect)
                
                
            }
            
            catCollected += 1
            self.catCollectedLabel.text = "CATS COLLECTED: \(self.catCollected)/5"
        
            
            // GAME OVER when zombie collects a cat
            // --------------------

            // 1. Initialize the new scene
//            let gameOverScene = GameOverScene(size:self.size)
//            gameOverScene.scaleMode = self.scaleMode
//
//            // 2. Configure the "animation" between screens
//            let transitionEffect = SKTransition.flipHorizontal(withDuration: 3)
//
//            // 3. Show the scene
//            self.view?.presentScene(gameOverScene, transition: transitionEffect)

            // if zombie touches cat, respawn a new cat in a random location
            // -----------------------------------------------------
            // 1. generate a new random x and y
            // arc4random_uniform(25) ---> generate a random number between (0,25)
            let randomX = arc4random_uniform(UInt32(self.size.width))
            
            // change the Y to choose numbers between (192,1344)
            // Example - to generate a number between 30, 50
            // arc4random(upper-lower) + lower
            // arc4random(50-30) + 30
            let randomY = arc4random_uniform(UInt32(self.actualPlayfieldRect.maxY - self.actualPlayfieldRect.minY)) + UInt32(self.actualPlayfieldRect.minY)

            // 2. move the cat to the new (x,y)
            self.cat.position.x = CGFloat(randomX)
            self.cat.position.y = CGFloat(randomY)

            // DEBUG:  output new position of cat
            print("Random (x,y): \(randomX), \(randomY)")
        
        }
        
        
        
        // @DEBUG - very useful for debugging sprite movements
//        print("Zombie position: \(self.zombie.position.x), \(self.zombie.position.y)")
    }
    
    
    
    // MARK: Detect when person touches the screen
    // Bitbucket commit #0f6944d
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        // 1. get the touch
        let locationTouched = touches.first
        if (locationTouched == nil) {
            // you can also use a if-let or guard statement here
            return
        }
        
        let mousePosition = locationTouched!.location(in: self)
        
        // 2. print out the (x,y) position of the touch
        print("Mouse is at: \(mousePosition.x), \(mousePosition.y)")
        
        // 3. implement the vector math algorithm to move zombie to person
        let a = mousePosition.x - self.zombie.position.x // (x2-x1)
        let b = mousePosition.y - self.zombie.position.y // (y2-y1)
        let h = sqrt((a*a) + (b*b))             // calculate hypotenuse
        
        self.xn = a/h           // xn = global class variable
        self.yn = b/h           // yn = global class variable
        
    }
}
